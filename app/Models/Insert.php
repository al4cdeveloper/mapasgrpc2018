<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Insert extends Model
{
	use Sluggable;
	
	protected $primaryKey="id_insert";
	protected $fillable = ['name_insert','token','file','slug'];

	public function sluggable(){
		return [
			'slug' => ['source' => 'name_insert']
		];
	}
}
