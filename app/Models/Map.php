<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
	
	protected $primaryKey="id_map";
	protected $fillable = ['name_map','token','type','file'];

}
