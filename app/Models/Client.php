<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Client extends Model
{
	use Sluggable;
	
	protected $primaryKey="id_client";
	protected $fillable = ['name_client','token','file','slug'];

	public function sluggable(){
		return [
			'slug' => ['source' => 'name_client']
		];
	}
}
