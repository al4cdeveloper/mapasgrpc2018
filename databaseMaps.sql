
-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Servidor: 45.40.164.81
-- Tiempo de generación: 05-04-2018 a las 14:11:56
-- Versión del servidor: 5.5.51
-- Versión de PHP: 5.1.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rutascolombia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `clients`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inserts`
--

CREATE TABLE `inserts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `inserts`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `maps`
--

CREATE TABLE `maps` (
  `id_map` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_map` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(131) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_map`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=144 ;

--
-- Volcar la base de datos para la tabla `maps`
--

INSERT INTO `maps` VALUES(44, 'Prueba', 'MP-8H0AnmOpnbkeG8OjCya4ePmLO', 'image', NULL, '2017-11-09 03:33:17', '2017-11-09 03:33:17');
INSERT INTO `maps` VALUES(45, 'Armenia', 'MP-tthckOGmoOWOmhg2Cwai0cPP7', 'image', 'mapas/ciudades/Armenia.jpg', '2017-11-09 19:32:01', '2017-11-09 19:32:01');
INSERT INTO `maps` VALUES(46, 'barranquilla', 'MP-AxXLHueaMruA8RZWNOj44mHBw', 'image', 'mapas/ciudades/Barranquilla.jpg', '2017-11-09 19:32:42', '2017-11-09 19:32:42');
INSERT INTO `maps` VALUES(47, 'bogota', 'MP-ZCJOrQpYObGipQ5RlGZcFV0te', 'image', 'mapas/ciudades/BOGOTa-DC.jpg', '2017-11-09 19:33:19', '2017-11-09 19:33:19');
INSERT INTO `maps` VALUES(48, 'bucaramanga', 'MP-5yAM97MMQAeH0dvbTOBJTs4zr', 'image', 'mapas/ciudades/Bucaramanga.jpg', '2017-11-09 19:33:49', '2017-11-09 19:33:49');
INSERT INTO `maps` VALUES(49, 'cali', 'MP-hfnhIBngc9qjWdawrRxEnIxbj', 'image', 'mapas/ciudades/cali.jpg', '2017-11-09 19:34:10', '2017-11-09 19:34:10');
INSERT INTO `maps` VALUES(50, 'cartagena', 'MP-g5MlFdCUAUDchTorkIoxSUZKs', 'image', 'mapas/ciudades/CARTAGENA.jpg', '2017-11-09 19:34:36', '2017-11-09 19:34:36');
INSERT INTO `maps` VALUES(51, 'cucuta', 'MP-D7abjurogN3tpY4kBgCvF4Pns', 'image', 'mapas/ciudades/cucuta.jpg', '2017-11-09 19:35:56', '2017-11-09 19:35:56');
INSERT INTO `maps` VALUES(52, 'ibague', 'MP-ngnn0P070bquDPKqTeQRjbszN', 'image', 'mapas/ciudades/Ibague.jpg', '2017-11-09 19:36:26', '2017-11-09 19:36:26');
INSERT INTO `maps` VALUES(53, 'manizales', 'MP-XfRNCkbSHyTwz0wLr1ogMPa0G', 'image', 'mapas/ciudades/Manizales.jpg', '2017-11-09 19:37:13', '2017-11-09 19:37:13');
INSERT INTO `maps` VALUES(54, 'medellin', 'MP-1jRuTEs8wfLQrEy0x5AxBlZDK', 'image', 'mapas/ciudades/MEDELLIN.jpg', '2017-11-09 19:37:52', '2017-11-09 19:37:52');
INSERT INTO `maps` VALUES(55, 'monteria', 'MP-gpzqpfsJ7WDLoLiEx86bH6IMG', 'image', 'mapas/ciudades/Monteria.jpg', '2017-11-09 19:38:22', '2017-11-09 19:38:22');
INSERT INTO `maps` VALUES(56, 'neiva', 'MP-go2f1MwqmWQCozJlcvJYNovWu', 'image', 'mapas/ciudades/Neiva-1.jpg', '2017-11-09 19:39:10', '2017-11-09 19:39:10');
INSERT INTO `maps` VALUES(57, 'pasto', 'MP-GDBpphFNjVO6rfso54NFN8SiS', 'image', 'mapas/ciudades/pasto.jpg', '2017-11-09 19:39:38', '2017-11-09 19:39:38');
INSERT INTO `maps` VALUES(58, 'popayan', 'MP-Q6gm2L3Fms5E7TY2I4tXxS2BF', 'image', 'mapas/ciudades/popayan.jpg', '2017-11-09 19:40:17', '2017-11-09 19:40:17');
INSERT INTO `maps` VALUES(59, 'providencia', 'MP-IpNAIFrPWNRIRx4jDJrwHueMY', 'image', 'mapas/ciudades/providencia.jpg', '2017-11-09 19:40:43', '2017-11-09 19:40:43');
INSERT INTO `maps` VALUES(60, 'puertocarreno', 'MP-cjQfVvXlix4YYTVMKDDiIXVro', 'image', 'mapas/ciudades/puerto_carreno.jpg', '2017-11-09 19:41:13', '2017-11-09 19:41:13');
INSERT INTO `maps` VALUES(61, 'riohacha', 'MP-s8SGVQSeG79c5vvCztBspnd21', 'image', 'mapas/ciudades/Rioacha.jpg', '2017-11-09 19:41:36', '2017-11-09 19:41:36');
INSERT INTO `maps` VALUES(62, 'sanandres', 'MP-wKYrcmTkfAf6tuNAEZF9uiJYT', 'image', 'mapas/ciudades/San-Andres-.jpg', '2017-11-09 19:42:03', '2017-11-09 19:42:03');
INSERT INTO `maps` VALUES(63, 'santamarta', 'MP-bohpriVbgnnDhITRYZksMV6MB', 'image', 'mapas/ciudades/Santa-Marta.jpg', '2017-11-09 19:42:39', '2017-11-09 19:42:39');
INSERT INTO `maps` VALUES(64, 'tunja', 'MP-ghTZgNbE4A5nwhDTFhana2m9m', 'image', 'mapas/ciudades/tunja.jpg', '2017-11-09 19:43:09', '2017-11-09 19:43:09');
INSERT INTO `maps` VALUES(65, 'valledupar', 'MP-OWiVITc0NbgBnUGYZ3ugGoVXz', 'image', 'mapas/ciudades/Valledupar.jpg', '2017-11-09 19:43:32', '2017-11-09 19:43:32');
INSERT INTO `maps` VALUES(66, 'villavicencio', 'MP-j7BFhYudhqWaCWYOcAb7g96jE', 'image', 'mapas/ciudades/VILLAVICENCIO.jpg', '2017-11-09 19:44:05', '2017-11-09 19:44:05');
INSERT INTO `maps` VALUES(67, 'antioquia', 'MP-udGRAyBut3g2JPQemZN3p2Kzh', 'image', 'mapas/departamentos/antioquia.jpg', '2017-11-15 14:28:18', '2017-11-15 14:28:18');
INSERT INTO `maps` VALUES(68, 'araucadep', 'MP-0YzYvisJYjjwVO0Y53HUTWhSJ', 'image', 'mapas/departamentos/arauca.jpg', '2017-11-15 14:29:06', '2017-11-15 14:29:06');
INSERT INTO `maps` VALUES(69, 'atlantico', 'MP-eUDxgyQxks4oKS3F33tdRxbQW', 'image', 'mapas/departamentos/atlantico.jpg', '2017-11-15 14:30:25', '2017-11-15 14:30:25');
INSERT INTO `maps` VALUES(70, 'bolivar', 'MP-LjPm7hA1U7itYQNqUcbMfQQjk', 'image', 'mapas/departamentos/bolivar.jpg', '2017-11-15 14:30:52', '2017-11-15 14:30:52');
INSERT INTO `maps` VALUES(71, 'boyaca', 'MP-4aRg0O1kEnrVYtP5Li3B6uwiF', 'image', 'mapas/departamentos/boyaca.jpg', '2017-11-15 14:31:46', '2017-11-15 14:31:46');
INSERT INTO `maps` VALUES(72, 'caldas', 'MP-jywaRRf1Iv2xxmbUO6ShWY3f2', 'image', 'mapas/departamentos/caldas.jpg', '2017-11-15 14:32:29', '2017-11-15 14:32:29');
INSERT INTO `maps` VALUES(73, 'cauca', 'MP-FlwbDcvbIF2zV4iq6PXt1Si7L', 'image', 'mapas/departamentos/cauca.jpg', '2017-11-15 14:32:56', '2017-11-15 14:32:56');
INSERT INTO `maps` VALUES(74, 'cesar', 'MP-z4JDjMiFitivZudFwNAB61IWY', 'image', 'mapas/departamentos/cesar.jpg', '2017-11-15 14:35:10', '2017-11-15 14:35:10');
INSERT INTO `maps` VALUES(75, 'choco', 'MP-bXRu5C39mGt8Z8rtqWsV97sXI', 'image', 'mapas/departamentos/choco.jpg', '2017-11-15 14:35:33', '2017-11-15 14:35:33');
INSERT INTO `maps` VALUES(76, 'cordoba', 'MP-33JLZIXWzr1bvbycEGbM7Ed37', 'image', 'mapas/departamentos/cordoba.jpg', '2017-11-15 14:35:58', '2017-11-15 14:35:58');
INSERT INTO `maps` VALUES(77, 'cundinamarca', 'MP-8dfAaXEdHpdpnaYPbaknIx1pI', 'image', 'mapas/departamentos/CUNDINAMARCA-1.jpg', '2017-11-15 14:36:28', '2017-11-15 14:36:28');
INSERT INTO `maps` VALUES(78, 'huila', 'MP-Nwn1Au9NJKYHocoOpNbzM0LXl', 'image', 'mapas/departamentos/huila.jpg', '2017-11-15 14:42:07', '2017-11-15 14:42:07');
INSERT INTO `maps` VALUES(79, 'guajira', 'MP-8GSa5BXCYYct80cSYUgbi4A5g', 'image', 'mapas/departamentos/la_guajira.jpg', '2017-11-15 14:42:29', '2017-11-15 14:42:29');
INSERT INTO `maps` VALUES(80, 'magdalena', 'MP-aShWPD5vvfB7cd6bqzjrMcqGt', 'image', 'mapas/departamentos/magdalena.jpg', '2017-11-15 14:42:58', '2017-11-15 14:42:58');
INSERT INTO `maps` VALUES(81, 'meta', 'MP-BZyc5OnX6jNJojfDUmQ8t2z3m', 'image', 'mapas/departamentos/meta.jpg', '2017-11-15 14:43:24', '2017-11-15 14:43:24');
INSERT INTO `maps` VALUES(82, 'nariño', 'MP-0Qyqw22wAfBpCyvVlekFuYzRP', 'image', 'mapas/departamentos/narino.jpg', '2017-11-15 14:43:57', '2017-11-15 14:43:57');
INSERT INTO `maps` VALUES(83, 'nortesantander', 'MP-IkRhodieMJLOLhp0SOCqjyMyT', 'image', 'mapas/departamentos/Norte-de-Santander.jpg', '2017-11-15 14:44:25', '2017-11-15 14:44:25');
INSERT INTO `maps` VALUES(84, 'quindio', 'MP-s3R2UGKfy2DLkRy3DnOUMPMAs', 'image', 'mapas/departamentos/quindio.jpg', '2017-11-15 14:44:50', '2017-11-15 14:44:50');
INSERT INTO `maps` VALUES(85, 'risaralda', 'MP-dU10tUswLuqsfG0hjMBblFOIt', 'image', 'mapas/departamentos/Risaralda.jpg', '2017-11-15 14:45:13', '2017-11-15 14:45:13');
INSERT INTO `maps` VALUES(86, 'santander', 'MP-Jujv5MJ0NJtHcZtHqWW6XeqJP', 'image', 'mapas/departamentos/santander.jpg', '2017-11-15 14:45:48', '2017-11-15 14:45:48');
INSERT INTO `maps` VALUES(87, 'sucre', 'MP-C4urNYaiiFn4poR8RzlR32h0Z', 'image', 'mapas/departamentos/sucre.jpg', '2017-11-15 14:46:12', '2017-11-15 14:46:12');
INSERT INTO `maps` VALUES(88, 'tolima', 'MP-oXdPG3rLySyw3RPJfU8DLgulB', 'image', 'mapas/departamentos/tolima.jpg', '2017-11-15 14:46:37', '2017-11-15 14:46:37');
INSERT INTO `maps` VALUES(89, 'vallecauca', 'MP-lpEDpD1mQQ3TiPrboYefNXuH5', 'image', 'mapas/departamentos/valle_del_cauca.jpg', '2017-11-15 14:47:02', '2017-11-15 14:47:02');
INSERT INTO `maps` VALUES(90, 'vichada', 'MP-7tmCPYXfCAEfC15t4ZLTrWipb', 'image', 'mapas/departamentos/vichada.jpg', '2017-11-15 14:47:59', '2017-11-15 14:47:59');
INSERT INTO `maps` VALUES(91, 'Bogota Centro Historico (BOGOTA)', 'MP-ph9BTBS8Raly6vS5qZvkInjTu', 'image', 'mapas/circuitos/Bogota-Centro-Historico.jpg', '2017-11-15 14:59:22', '2017-11-15 14:59:22');
INSERT INTO `maps` VALUES(92, 'BOGOTA PARQUES correccion (BOGOTÁ)', 'MP-iTZhDgGVpiP0aXSljrsOkyej3', 'image', 'mapas/circuitos/BOGOTA-PARQUES.jpg', '2017-11-15 15:00:04', '2017-11-15 15:00:04');
INSERT INTO `maps` VALUES(93, 'CIRCUITO ALREDEDORES B-MANGA (BUCARAMANGA)', 'MP-ROf3UBC0Ui4w3CcH6PqQtB2zG', 'image', 'mapas/circuitos/CIRCUITO-ALREDEDORES-B-MANGA.jpg', '2017-11-15 15:17:38', '2017-11-15 15:17:38');
INSERT INTO `maps` VALUES(94, 'CIRCUITO ALREDEDORES MEDELLIN (ANTIOQUIA)', 'MP-OZwcj437k62WI2Q17n4JALPqc', 'image', 'mapas/circuitos/CIRCUITO-ALREDEDORES-MEDELLIN.jpg', '2017-11-15 15:18:17', '2017-11-15 15:18:17');
INSERT INTO `maps` VALUES(95, 'CIRCUITO ARMENIA - MONTENEGRO - QUIMBAYA - FILANDIA (QUINDÍO)', 'MP-GTOItuws0JL5MTpTVmBYdC5AH', 'image', 'mapas/circuitos/CIRCUITO-ARMENIA---MONTENEGRO---QUIMBAYA---FILANDIA.jpg', '2017-11-15 15:19:06', '2017-11-15 15:19:06');
INSERT INTO `maps` VALUES(96, 'CIRCUITO BOGOTÁ GIRARDOT (CUNDINAMARCA)', 'MP-Uf0kSydFgotPNthTnJGn0f0Gh', 'image', 'mapas/circuitos/CIRCUITO-BOGOTa-GIRARDOT.jpg', '2017-11-15 15:23:18', '2017-11-15 15:23:18');
INSERT INTO `maps` VALUES(97, 'CIRCUITO BOGOTÁ TOBIA (CUNDINAMARCA)', 'MP-J3qcV9vUGA5s7RoAS3SpC9GAq', 'image', 'mapas/circuitos/CIRCUITO-BOGOTa-TOBIA.jpg', '2017-11-15 15:24:10', '2017-11-15 15:24:10');
INSERT INTO `maps` VALUES(98, 'CIRCUITO CALERA SOPO (CUNDINAMARCA)', 'MP-wOejXOecOy5nMLDaesYIKR5uy', 'image', 'mapas/circuitos/CIRCUITO-CALERA-SOPO.jpg', '2017-11-15 15:24:56', '2017-11-15 15:24:56');
INSERT INTO `maps` VALUES(99, 'CIRCUITO CHIA-TENJO(CUNDINAMARCA)', 'MP-AAiq90acqn8AS2gs8JA07aSa3', 'image', 'mapas/circuitos/CIRCUITO-CHIA-TENJO.jpg', '2017-11-15 15:25:34', '2017-11-15 15:25:34');
INSERT INTO `maps` VALUES(100, 'CIRCUITO DESIERTO DE LA TATACOA (HUILA)', 'MP-tuNub50ZkkX9y9Y7flT0Y4fr2', 'image', 'mapas/circuitos/CIRCUITO-DESIERTO-DE-LA-TATACOA.jpg', '2017-11-15 15:26:25', '2017-11-15 15:26:25');
INSERT INTO `maps` VALUES(101, 'CIRCUITO IBAGUE - REPRESA DE HIDROPRADO (TOLIMA)', 'MP-RckNtNfPjUE22Iv3oUtjB1EWE', 'image', 'mapas/circuitos/CIRCUITO-IBAGUE---REPRESA-DE-HIDROPRADO.jpg', '2017-11-15 15:34:34', '2017-11-15 15:34:34');
INSERT INTO `maps` VALUES(102, 'CIRCUITO LA COCHA (NARIÑO)', 'MP-sscOVAG7VuAIKq1E54GNzJbud', 'image', 'mapas/circuitos/CIRCUITO-LA-CONCHA.jpg', '2017-11-15 15:35:27', '2017-11-15 15:35:27');
INSERT INTO `maps` VALUES(103, 'CIRCUITO LAGO CALIMA (VALLE DEL CAUCA)', 'MP-v6f92TCu6rqG7yBB8jlzl0EqG', 'image', 'mapas/circuitos/CIRCUITO-LAGO-CALIMA.jpg', '2017-11-15 15:35:57', '2017-11-15 15:35:57');
INSERT INTO `maps` VALUES(104, 'CIRCUITO LAGO DE TOTA (BOYACÁ)', 'MP-s0qEuD9ATjCMV7Tmxzu5b6evs', 'image', 'mapas/circuitos/CIRCUITO-LAGO-DE-TOTA.jpg', '2017-11-15 15:36:39', '2017-11-15 15:36:39');
INSERT INTO `maps` VALUES(105, 'CIRCUITO NORTE DEL VALLE (VALLE DEL CAUCA)', 'MP-OQstg8VgzALcJl53YSZ5LmDlQ', 'image', 'mapas/circuitos/CIRCUITO-NORTE-DEL-VALLE.jpg', '2017-11-15 15:37:08', '2017-11-15 15:37:08');
INSERT INTO `maps` VALUES(106, 'Circuito Nuevo2017', 'MP-JwWY1pMSRf9ZbqzLbLvwRyvKy', 'image', 'mapas/circuitos/Circuito-Nuevo2017.jpg', '2017-11-15 15:38:02', '2017-11-15 15:38:02');
INSERT INTO `maps` VALUES(107, 'CIRCUITO PARQUE TAYRONA (SANTA MARTA)', 'MP-AwUeRKXoHVq6IiYXsY9SxV4jr', 'image', 'mapas/circuitos/CIRCUITO-PARQUE-TAYRONA.jpg', '2017-11-15 15:38:28', '2017-11-15 15:38:28');
INSERT INTO `maps` VALUES(108, 'CIRCUITO PEREIRA PARQUE DEL OTUN (PEREIRA)', 'MP-Ba06VyHrsVjdSHVO82wr0uTYE', 'image', 'mapas/circuitos/CIRCUITO-PARQUE-DEL-OTUN.jpg', '2017-11-15 15:39:22', '2017-11-15 15:39:22');
INSERT INTO `maps` VALUES(109, 'CIRCUITO PEREIRA SANTA ROSA (RISARALDA)', 'MP-MvzRO1sZ1zVzgm2bGf4oaSwcp', 'image', 'mapas/circuitos/CIRCUITO-PEREIRA-SANTAROSA.jpg', '2017-11-15 15:39:57', '2017-11-15 15:39:57');
INSERT INTO `maps` VALUES(110, 'CIRCUITO PNN AMACAYACU', 'MP-dTRbxEH7vw8Yw9xrJNOLZu13S', 'image', 'mapas/circuitos/CIRCUITO-PNN-AMACAYACU.jpg', '2017-11-15 15:40:29', '2017-11-15 15:40:29');
INSERT INTO `maps` VALUES(111, 'CIRCUITO PNN LOS NEVADOS (MANIZALES)', 'MP-bWpommCfeONTvVo23nydU0WIO', 'image', 'mapas/circuitos/CIRCUITO-PNN-LOS-NEVADOS.jpg', '2017-11-15 15:40:58', '2017-11-15 15:40:58');
INSERT INTO `maps` VALUES(112, 'CIRCUITO SAN AGUSTIN (HUILA)', 'MP-IHjJLbVIAk4XWkcK75g2uj6RR', 'image', 'mapas/circuitos/CIRCUITO-SAN-AGUSTIN.jpg', '2017-11-15 15:41:27', '2017-11-15 15:41:27');
INSERT INTO `maps` VALUES(113, 'CIRCUITO SOCORRO SAN GIL BARICHARA LOS SANTOS (SANTANDER)', 'MP-jMSguHZb0IXcEFMYJKU4WFb1W', 'image', 'mapas/circuitos/CIRCUITO-SOCORRO-SAN-GIL-BARICHARA-LOS-SANTOS.jpg', '2017-11-15 15:47:11', '2017-11-15 15:47:11');
INSERT INTO `maps` VALUES(114, 'CIRCUITO TUNJA VILLA DE LEYVA CHIQUINQUIRÁ (BOYACÁ)', 'MP-ewfln7F9ZVEGUPHDNThsFfcqa', 'image', 'mapas/circuitos/CIRCUITO--TUNJA-VILLA-DE-LEYVA-CHIQUINQUIRa.jpg', '2017-11-15 15:48:12', '2017-11-15 15:48:12');
INSERT INTO `maps` VALUES(115, 'ruta1', 'MP-9iLaPcG3Gx9XtNeFtE8l10Plj', 'image', 'mapas/rutas/Ruta_1.jpg', '2017-12-02 01:37:17', '2017-12-02 01:37:17');
INSERT INTO `maps` VALUES(116, 'ruta2', 'MP-ID4xHrH0cRQoyU5533zRhelWn', 'image', 'mapas/rutas/Ruta_2.jpg', '2017-12-02 01:38:17', '2017-12-02 01:38:17');
INSERT INTO `maps` VALUES(117, 'ruta3', 'MP-GXowiHfVMMDduEpluOUpTZsWz', 'image', 'mapas/rutas/Ruta_3.jpg', '2017-12-02 01:38:42', '2017-12-02 01:38:42');
INSERT INTO `maps` VALUES(118, 'ruta4', 'MP-keOFabm8zSrh7m4U0hoFGJauD', 'image', 'mapas/rutas/Ruta_4.jpg', '2017-12-02 01:39:17', '2017-12-02 01:39:17');
INSERT INTO `maps` VALUES(119, 'ruta5', 'MP-zoD1kdmy11JdnRMfi4nF9hGqF', 'image', 'mapas/rutas/Ruta_5.jpg', '2017-12-02 01:39:46', '2017-12-02 01:39:46');
INSERT INTO `maps` VALUES(120, 'ruta6', 'MP-l7pvC340G6lTsTVuD8RuV7M0u', 'image', 'mapas/rutas/Ruta_6.jpg', '2017-12-02 01:40:12', '2017-12-02 01:40:12');
INSERT INTO `maps` VALUES(121, 'ruta7', 'MP-s9L8zrtHQYjT3kA9FuCzp6cyX', 'image', 'mapas/rutas/Ruta_7.jpg', '2017-12-02 01:40:38', '2017-12-02 01:40:38');
INSERT INTO `maps` VALUES(122, 'ruta8', 'MP-Gt5ttzVCl3cMxTDvcxyw8IbCk', 'image', 'mapas/rutas/Ruta_8.jpg', '2017-12-02 01:41:11', '2017-12-02 01:41:11');
INSERT INTO `maps` VALUES(123, 'ruta9', 'MP-L2qXBoE4t8x34ao8mbFfPbrmK', 'image', 'mapas/rutas/Ruta_9.jpg', '2017-12-02 01:41:38', '2017-12-02 01:41:38');
INSERT INTO `maps` VALUES(124, 'ruta10', 'MP-Yusa7NVadTLCyQ5Gn9KxxSUJx', 'image', 'mapas/rutas/Ruta_10.jpg', '2017-12-02 01:42:15', '2017-12-02 01:42:15');
INSERT INTO `maps` VALUES(125, 'ruta11', 'MP-9yIBVtzqVJyIFJWyvy7lENJNy', 'image', 'mapas/rutas/Ruta_11.jpg', '2017-12-02 01:42:47', '2017-12-02 01:42:47');
INSERT INTO `maps` VALUES(126, 'ruta12', 'MP-hlqb4YkDHVzav06fyOUiKtNjA', 'image', 'mapas/rutas/Ruta_12.jpg', '2017-12-02 01:43:19', '2017-12-02 01:43:19');
INSERT INTO `maps` VALUES(127, 'ruta13', 'MP-9XnSLV96mlblGO2Bod7ojmW7g', 'image', 'mapas/rutas/Ruta_13.jpg', '2017-12-02 01:43:56', '2017-12-02 01:43:56');
INSERT INTO `maps` VALUES(128, 'ruta14', 'MP-fSK2ckb9I4VEe11zcmf1oRpCY', 'image', 'mapas/rutas/Ruta_14.jpg', '2017-12-02 01:44:31', '2017-12-02 01:44:31');
INSERT INTO `maps` VALUES(129, 'ruta15', 'MP-NVkK3BZVl27Gehpid3we46rrl', 'image', 'mapas/rutas/Ruta_15.jpg', '2017-12-02 01:45:00', '2017-12-02 01:45:00');
INSERT INTO `maps` VALUES(130, 'ruta16', 'MP-sQdSsbGnwrq8qmutuaHMAZZDw', 'image', 'mapas/rutas/Ruta_16.jpg', '2017-12-02 01:45:49', '2017-12-02 01:45:49');
INSERT INTO `maps` VALUES(131, 'ruta17', 'MP-eICF9X80b0smGPT7g2yCw27HJ', 'image', 'mapas/rutas/Ruta_17.jpg', '2017-12-02 01:46:21', '2017-12-02 01:46:21');
INSERT INTO `maps` VALUES(132, 'ruta18', 'MP-ThJTUf7DRMNPUN0VgnC6hKmjj', 'image', 'mapas/rutas/Ruta_18.jpg', '2017-12-02 01:46:50', '2017-12-02 01:46:50');
INSERT INTO `maps` VALUES(133, 'ruta19', 'MP-ZPl6w40NNTI31mUN9KIXKEd8g', 'image', 'mapas/rutas/Ruta_19.jpg', '2017-12-02 01:47:26', '2017-12-02 01:47:26');
INSERT INTO `maps` VALUES(134, 'pereira', 'MP-JavlgJIBJUKYCKjlcqyMn8VJl', 'image', 'mapas/ciudades/PEREIRA2.jpg', '2017-12-05 01:57:37', '2017-12-05 01:57:37');
INSERT INTO `maps` VALUES(135, 'norte de medellin', 'MP-H23RbYWm5knPIfnG9OnQUe1s1', 'image', 'mapas/circuitos/CIRCUITO-NORTE-MEDELLIN.jpg', '2017-12-05 02:31:45', '2017-12-05 02:31:45');
INSERT INTO `maps` VALUES(136, 'idecut', 'MP-NSxplWCoCQ4lXwVyxjKh9ZiNV', 'pdf', 'mapas/insertos/elDoradoCundinamarca.pdf', '2017-12-06 19:33:35', '2017-12-06 19:33:35');
INSERT INTO `maps` VALUES(137, 'separataidecut', 'MP-aoJVX8JPF9bBLzeCDzzav8HOS', 'pdf', 'mapas/insertos/CundinamarcaTuristica.pdf', '2017-12-06 19:33:52', '2017-12-06 19:33:52');
INSERT INTO `maps` VALUES(138, 'mapaidecut', 'MP-ZYShLNsawn7F6Wlg8W1IbDlLd', 'pdf', 'mapas/insertos/elDoradoCundinamarca.pdf', '2017-12-06 19:34:21', '2017-12-06 19:34:21');
INSERT INTO `maps` VALUES(139, 'feriasyfiestas', 'MP-wgle5ed36vPUXZql65s3qIbnJ', 'pdf', 'mapas/insertos/FeriasyFiestas.pdf', '2017-12-06 19:34:51', '2017-12-06 19:34:51');
INSERT INTO `maps` VALUES(140, 'avesmincit', 'MP-TynfkBMAW0FbeJiKycHyC3EIv', 'pdf', 'mapas/insertos/FONTUR.pdf', '2017-12-06 19:35:17', '2017-12-06 19:35:17');
INSERT INTO `maps` VALUES(141, 'Seguridad Vial', 'MP-0r1bedfYCjvN559bDwKBI7eH0', 'pdf', 'mapas/insertos/ANSV.pdf', '2017-12-14 20:15:21', '2017-12-14 20:15:21');
INSERT INTO `maps` VALUES(142, 'Secretaria de hacienda cundinamarca', 'MP-VqZuqZvR0H6eX5QgBDmGNxkji', 'pdf', 'mapas/insertos/SecretariadeHacienda.pdf', '2017-12-26 10:06:25', '2017-12-14 20:17:29');
INSERT INTO `maps` VALUES(143, 'Triptico', 'MP-AT2tq1o1i2BGYbM68HLqWkNsK', 'pdf', 'mapas/insertos/Bogota2018.pdf', '2017-12-15 14:35:32', '2017-12-15 14:35:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=19 ;

--
-- Volcar la base de datos para la tabla `migrations`
--

INSERT INTO `migrations` VALUES(14, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES(15, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES(16, '2017_11_08_210452_create_maps_table', 1);
INSERT INTO `migrations` VALUES(17, '2017_11_08_214817_create_inserts_table', 1);
INSERT INTO `migrations` VALUES(18, '2017_11_08_214903_create_clients_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcar la base de datos para la tabla `password_resets`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

--
-- Volcar la base de datos para la tabla `users`
--

