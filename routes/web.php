<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('newqr','QRController@index');
Route::post('storeqr','QRController@store');
Route::get('fastqr',function(){
	return view('fastqr');
});
Route::get('detailmap/{code}','QRController@scanMap');