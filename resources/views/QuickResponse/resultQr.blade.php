<!DOCTYPE html>
<html>
<head>
	<title>Nuevo QR</title>
	<style type="text/css">{{asset('css/app.css')}}</style>
</head>
<body>
	<h1>El mapa {{$map->name_map}} Se ha registrado correctamente</h1>
	<h4>Su código es: {{$map->token}}</h4>
 	<a href="{{url('/newqr')}}">Volver</a>
{{-- {!!QrCode::size(100)->merge('https://upload.wikimedia.org/wikipedia/commons/6/6f/HP_logo_630x630.png', .3, true)->generate('hola');!!}
	{!! QrCode::size(100)->generate('http://rutascolombia.com/mapasgrpc18/detailmap/'.$map->token); !!}
 --}}
 <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(500)->merge('http://rutascolombia.com/mapasgrpc18/grpc-blue2.png', .3, true)->errorCorrection('H')->generate('http://rutascolombia.com/mapasgrpc18/detailmap/'.$map->token)); !!} ">
</body>
</html>