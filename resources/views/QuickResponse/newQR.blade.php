<!DOCTYPE html>
<html>
<head>
	<title>Nuevo QR</title>
	<style type="text/css">{{asset('css/app.css')}}</style>
</head>
<body>
	<h1>Crear nuevo QR de mapas</h1>
	<form method="post" action="{{url('storeqr')}}">
		{{csrf_field()}}
		<div>
			<label for="name_map">Nombre del lugar:</label>
			<input type="text" name="name_map" id="name_map" required=>
		</div>
		<button type="submit">Registrar</button>
	</form>
	<script src="{{asset('js/app.js')}}"></script>
</body>
</html>