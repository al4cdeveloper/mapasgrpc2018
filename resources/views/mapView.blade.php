<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TNTNQRB');</script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,400italic'>
    <link rel="stylesheet" type="text/css" href="{{asset('map/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('map/font-awesome/css/font-awesome.min.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{asset('map/css/style.css')}}">

    <link rel="shortcut icon" href="{{asset('map/images/icon/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon57.png')}}" sizes="57x57">
    <link rel="apple-touch-icno" href="{{asset('map/images/icon/icon72.png')}}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon76.png')}}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon114.png')}}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon120.png')}}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon144.png')}}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon152.png')}}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{asset('map/images/icon/icon180.png')}}" sizes="180x180">



    <title>Mapas RutasColombia</title>

</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNTNQRB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) --> 
    <div class="main-body">	
        <div id="menu">
            <div class="container">
                <div class="col-md-2 col-md-offset-2 col-sm-4 col-xs-4">
                <img src="{{asset('map/images/logorutas.png')}}" alt="" style="height: auto; " class="img-responsive">
                </div>
                <div class="col-md-5 col-sm-6">
                <img src="{{asset('map/images/logo_medianoM.png')}}" alt="" style="height: auto; " class="img-responsive">
                </div>
                <div class="col-md-2 col-sm-2" align="center" style="font-size: 28px;margin-top: 40px;">
                    <a href="http://www.rutascolombia.com" style="color: white;">Inicio</a>
                </div>
            </div>
        </div>      
        <div class="container">
            <h1 align="center" style="color:#d21616;">{{$map->name_map}}   </h1>
            <div class="row">      
                <div class="main-page">
                    <div class="content-main">
                        <div class="row margin-b-30">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" align="center">

                                @if($map->type=="image")
                                    <img src="{{asset($map->file)}}" alt="Image" class="img-responsive" style="border-top: 7px solid #10a5cd;border-bottom: 7px solid #00cf00   ;">     
                                @else              
                                    <iframe src="{{asset($map->file)}}" style="width:100%; height:800px;border-top: 7px solid #10a5cd;border-bottom: 7px solid #00cf00   ;" frameborder="0">
                                        
                                    </iframe>
                                @endif

                            </div>    
                        </div>
                    </div> <!-- .content-main -->
                </div> <!-- .main-page -->
            </div> <!-- .row -->           
            <footer class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer">
                    <p class="copyright">Copyright © 2017 Rutas Colombia 
                    </p>
                </div>    
            </footer>  <!-- .row -->      
        </div> <!-- .container -->
    </div> <!-- .main-body -->

    <!-- JavaScript -->
    <script src="{{asset('map/js/jquery-1.11.3.min.js')}}"></script>
    <script src="{{asset('map/js/bootstrap.min.js')}}"></script>

</body>
</html>
